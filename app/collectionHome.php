<?php 
// Se va a crear los Arreglos

$category = array(

    0 => array(
        "title" => 'Women',
        "image" => 'collection-img1-446x213.jpg',
    ),

    1 => array(
        "title" => 'Man',
        "image" => 'collection-img2-446x213.jpg',
    ),

    2 => array(
        "title" => 'Accesories',
        "image" => 'collection-img3-446x213.jpg'
    ),

);

$product = array(

    0 => array(

        "image" => 'product1-270x340.jpg',
        "productName" => 'Licity jelly leg flat Sandals',
        "price" => '$23.00'

    ),

    1 => array(
        "image" => 'product2-270x340.jpg',
        "productName" => 'Giraffe Print T-Shirt',
        "price" => '$35.00'
    ),

    2 => array(
        "image" => 'product3-270x340.jpg"',
        "productName" => 'Stripe Print Maxi Dress',
        "price" => '$85.00'
    ),

    3 => array(
        "image" => 'product4-270x340.jpg"',
        "productName" => 'Blue sun top in grid top',
        "price" => '$35.00'
    ),

    4 => array(
        "image" => 'product6-270x340.jpg"',
        "productName" => 'Slim Fit Stretch Wool Blazer',
        "price" => '$35.00'
    ),

    5 => array(
        "image" => 'product7-270x340.jpg"',
        "productName" => 'Stripe Print Maxi Dress',
        "price" => '$35.00'
    ),

    6 => array(
        "image" => 'product8-270x340.jpg"',
        "productName" => 'Blue sun top in grid top',
        "price" => '$35.00'
    ),

    7 => array(
        "image" => 'product8-270x340.jpg"',
        "productName" => 'Blue sun top in grid top',
        "price" => '$35.00'
    ),

);

$newArrival = array(
    0 => array(
        "subtitle" => 'New Arrivals',
        "mainTitle" => 'Rose Print Ripped Tee for Women',
        "image" => 'new-arrivals-add-719x374.jpg',
    ),

    1 => array(
        "subtitle" => 'Discover Collection',
        "mainTitle" => 'Capsule Wardrobe For Men',
        "image" => 'discover-collection-add-719x374.jpg',
    ),
);

$newArrival = array(
    0 => array(
        "subtitle" => 'New Arrivals',
        "mainTitle" => 'Rose Print Ripped Tee for Women',
        "image" => 'new-arrivals-add-719x374.jpg',
    ),

    1 => array(
        "subtitle" => 'Discover Collection',
        "mainTitle" => 'Capsule Wardrobe For Men',
        "image" => 'discover-collection-add-719x374.jpg',
    ),
);

$newCloth = array(
    0 => array(
        "image" => 'product4-270x340.jpg"',
        "productName" => 'Blue sun top in grid top',
        "price" => '$35.00',
    ),

    1 => array(
        "image" => 'product7-270x340.jpg"',
        "productName" => 'dicen top',
        "price" => '$45.00',
    ),

    2 => array(
        "image" => 'product2-270x340.jpg"',
        "productName" => 'winatop',
        "price" => '$45.00',
    ),
);

$post = array(
    0 => array(
        "image" => 'blog-img1-370x268.jpg"',
        "postDate" => '08 July 2019',
        "autor" => 'John Due',
        "title" => 'Essentially unchanged Popularise',
        "text" => 'when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged',
    ),

    1 => array(
        "image" => 'blog-img2-370x268.jpg"',
        "postDate" => '08 July 2019',
        "autor" => 'John Due',
        "title" => 'Various versions have evolved over',
        "text" => 'when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged',
    ),

    2 => array(
        "image" => 'blog-img3-370x268.jpg"',
        "postDate" => '08 July 2019',
        "autor" => 'John Due',
        "title" => 'Passages of Lorem Ipsum available',
        "text" => 'when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged',
    ),
);


?>