<section class="abt-banner">
			<div class="container">
				<div class="abt-banner-content">
					<h2 class="breadcrumb-heading"> <?= $title ?></h2>
					<div class="breadcrumb">
						<div class="breadcrumb-item"><a href="http://localhost/modishphp/"><?= $home ?></a></div>
						<div class="breadcrumb-item active"><?= $title ?></div>
					</div>
				</div>
			</div>
		</section>