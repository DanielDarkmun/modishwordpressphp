<meta charset="utf-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge" />
		<title>Modish - Responsive Fashion Store | HTML5, Bootstrap 4 Template</title>
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
		<meta name="description" content="Modish - Responsive Fashion Store | HTML5, Bootstrap 4 Template" />
		<meta name="author"	content="" />
		<!-- Place favicon.ico in the root directory -->
		<link rel="shortcut icon" href="<?= BASE_PATH ?>public/images/favicon.png" type="image/x-icon" />
		<!-- Google Font -->
		<link href="https://fonts.googleapis.com/css?family=Rubik:300,400,500,700,900&amp;display=swap" rel="stylesheet" /> 

		<!-- font-family: 'Rubik', sans-serif; -->

		<!-- Place css here -->
		<!-- bootstrap v4.3.1 css -->
		<link href="<?= BASE_PATH ?>public/css/bootstrap.min.css" rel="stylesheet" />
		<!--Owl carousel CSS-->
		<link rel="stylesheet" href="<?= BASE_PATH ?>public/css/owl.carousel.min.css" />
		<link rel="stylesheet" href="<?= BASE_PATH ?>public/css/owl.theme.default.min.css" />
		<!--Fancy Scroll Bar-->
		<link rel="stylesheet" href="<?= BASE_PATH ?>public/css/jquery.mCustomScrollbar.min.css" />
		<!-- Fontello Icon -->
		<link rel="stylesheet" href="<?= BASE_PATH ?>public/css/fontello.css" />
		<!-- All common css of Theme -->
		<link rel="stylesheet" href="<?= BASE_PATH ?>public/css/default.css" />
		<!--Theme megamenu css -->
		<link rel="stylesheet" href="<?= BASE_PATH ?>public/css/megamenu.css" />
		<!--Theme style css -->
		<link rel="stylesheet" href="<?= BASE_PATH ?>public/css/style.css" />
		<link rel="stylesheet" href="<?= BASE_PATH ?>public/css/query.css" />
