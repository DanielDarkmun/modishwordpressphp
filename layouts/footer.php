<footer class="footer-main footer-style-1">
    		<div class="top-footer">
	    		<div class="container">
	    			<div class="row">
		    			<div class="col-12 col-sm-12 col-md-12 col-lg-5 col-xl-6">
		    				<img class="img-fluid footer-logo" src="<?= BASE_PATH ?>public/images/footer-logo.png" alt="Modish HTML5 & Bootstrap 4 Theme" />
		    				<p class="about_txt">Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock..</p>
		    				<div class="footer-widget">
			    				<div class="widget-content social">
									<ul>
										<li><a href="#" target="_blank"><i class="icon-facebook-logo"></i></a></li>
										<li><a href="#" target="_blank"><i class="icon-twitter"></i></a></li>
										<li><a href="#" target="_blank"><i class="icon-pinterest"></i></a></li>
										<li><a href="#" target="_blank"><i class="icon-youtube"></i></a></li>
										<li><a href="#" target="_blank"><i class="icon-google-plus-social-logotype"></i></a></li>
									</ul>
								</div>
							</div>
		    			</div>
		    			<div class="col-12 col-sm-12 col-md-12 col-lg-7 col-xl-6">
		    				<div class="row">
		    					<div class="col-12 col-sm-4">
		    						<div class="footer-widget">
		    							<div class="widget-tit store-time">
		    								<h4>Opening Time</h4>
		    							</div>
		    							<div class="widget-content">
		    								<ul>
		    									<li>Mon - Fri: 8AM - 10PM</li>
		    									<li>Sat: 9AM-8PM</li>
		    									<li>Sun: Closed</li>
		    								</ul>
		    							</div>
		    						</div>
		    					</div>
		    					<div class="col-12 col-sm-4">
		    						<div class="footer-widget">
		    							<div class="widget-tit">
		    								<h4>Information</h4>
		    							</div>
		    							<div class="widget-content information">
		    								<ul>
		    									<li><a href="#">About Us</a></li>
		    									<li><a href="#">Contact us</a></li>
		    									<li><a href="#">New Collection</a></li>
		    									<li><a href="#">Term & Condition</a></li>
		    									<li><a href="#">Privacy Policy</a></li>
		    								</ul>
		    							</div>
		    						</div>
		    					</div>
		    					<div class="col-12 col-sm-4">
		    						<div class="footer-widget">
		    							<div class="widget-tit">
		    								<h4>Shop Guide</h4>
		    							</div>
		    							<div class="widget-content guide">
		    								<ul>
		    									<li><a href="#">Order Tracking</a></li>
		    									<li><a href="#">My Account</a></li>
		    									<li><a href="#">Size Guide</a></li>
		    									<li><a href="#">Terms & Condition</a></li>
		    									<li><a href="#">FAQs</a></li>
		    								</ul>
		    							</div>
		    						</div>
		    					</div>
		    				</div>
		    			</div>
	    			</div>
	    		</div>
    		</div>
    		<div class="bottom-footer">
    			<div class="container">
    				<div class="row">
    					<div class="col-12 col-sm-12 col-md-12 col-lg-6">
    						<div class="copyright">COPYRIGHT @ 2019 All rights reserved</div>
    					</div>
    					<div class="col-12 col-sm-12 col-md-12 col-lg-6">
    						<div class="design">Website designed & Developed by <a target="_blank" href="https://www.ncodetechnologies.com/">NCode Technologies, Inc.</a></div>
    					</div>
    				</div>
    			</div>
    		</div>
    	</footer>